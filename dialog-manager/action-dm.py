import os
import time

from jacolib import comm_tools, extra_tools, utils

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(file_path + "../")

input_wake_word = "Jaco/WakeWord"
toggle_asr_topic = "Jaco/AsrToggle"
input_slu_topic = "Jaco/RecognizedIntent"
input_say_topic = "Jaco/Skills/SayText"
output_say_topic = "Jaco/SayText"
say_topic_status = "Jaco/StreamWav/Status"
output_wav_base = "Jaco/{}/AudioWav"

mqtt_client = None

first_satellite: dict = {}
sats_blocked = False
sat_wait_time = 0.1
sat_reset_time = 1

question_dicts: dict = {}

# Sounds for start/finished/error signals
hint_pause_time = 0.5
asr_sta_wav = extra_tools.wav_to_base64(file_path + "../media/sounds/asr_started.wav")
asr_fin_wav = extra_tools.wav_to_base64(file_path + "../media/sounds/asr_finished.wav")
slu_err_wav = extra_tools.wav_to_base64(file_path + "../media/sounds/asr_error.wav")

session_running_at: dict = {}
session_timeout = utils.load_global_config()["speech_transcription_timeout"] * 2
ignore_wakeword_in_session = True


# ==================================================================================================


def on_connect(client):
    client.subscribe(input_wake_word)
    client.subscribe(input_slu_topic)
    client.subscribe(say_topic_status)
    client.subscribe(toggle_asr_topic)

    # Subscribe to all SayText skill topics
    for t in comm_tools.get_all_topics():
        if t.startswith("Jaco/Skills/SayText"):
            client.subscribe(t)


# ==================================================================================================


def on_message(client, userdata, msg):
    if msg.topic == input_wake_word:
        on_wakeword(client, msg)

    elif msg.topic == input_slu_topic:
        on_slu_intent(client, msg)

    elif msg.topic.startswith("Jaco/Skills/SayText"):
        on_say_text(client, msg)

    elif msg.topic == say_topic_status:
        on_say_status_update(client, msg)

    elif msg.topic == toggle_asr_topic:
        on_toggle_asr(client, msg)


# ==================================================================================================


def on_wakeword(client, msg):
    """Wait short time and check which satellite was the fastest to detect the wakeword, this one
    will be selected for stt input as it's assumed to be nearest to the user"""

    global first_satellite, sats_blocked

    payload = comm_tools.decrypt_msg(msg.payload, msg.topic)

    if sats_blocked:
        # This satellite was too slow
        return

    if ignore_wakeword_in_session and payload["satellite"] in session_running_at:
        # This satellite is blocked by a running session
        if session_running_at[payload["satellite"]] > time.time() - session_timeout:
            msg = "Ignoring wake word detection at '{}' because a session is running there"
            print(msg.format(payload["satellite"]))
            return

        # Unblock because timeout was reached
        msg = "Unblocked {} because session timeout was reached"
        print(msg.format(payload["satellite"]))

    if first_satellite == {} or payload["timestamp"] < first_satellite["timestamp"]:
        first_satellite = payload

    # Give satellites with slower network connection some time to send their detection
    for _ in range(int(sat_wait_time / 0.001)):
        time.sleep(0.001)

    if sats_blocked:
        # The earliest (nearest) satellite is already chosen
        return
    sats_blocked = True

    # Activate the speech to text module now
    activate_stt(client, first_satellite["satellite"])

    # Wait some time before handling wake word detections again, so even very slow satellites
    #  had enough time to send their message
    # If a skill needs long time to response, it's possible that the user already said the wake
    #  word again, the response will then be spoken on the new satellite. This is on purpose and
    #  allows the user to start multiple parallel running skills. It also solves problems with
    #  skills not responding because of some internal error by ignoring them.
    time.sleep(sat_reset_time)
    sats_blocked = False


# ==================================================================================================


def activate_stt(client, satellite):
    """Activate the speech to text module by sending a toggle message"""
    global session_running_at

    # Save satellite to be able to block wakeword detections
    session_running_at[satellite] = time.time()

    # Play asr starting sound
    pld = {
        "data": asr_sta_wav,
        "timestamp": time.time(),
        "type": "input_status",
    }
    output_topic = output_wav_base.format(satellite)
    msg_out = comm_tools.encrypt_msg(pld, output_topic)
    client.publish(output_topic, msg_out)
    client.loop_write()
    print("Published start sound")
    time.sleep(hint_pause_time)

    payload = {
        "toggle": True,
        "satellite": satellite,
    }
    msg_out = comm_tools.encrypt_msg(payload, toggle_asr_topic)
    client.publish(toggle_asr_topic, msg_out)
    client.loop_write()
    print("Published a toggle request:", payload)


# ==================================================================================================


def on_toggle_asr(client, msg):
    """Publish asr finished sound"""

    payload = comm_tools.decrypt_msg(msg.payload, msg.topic)
    if not payload["toggle"] is False:
        return

    # Play asr finished sound
    pld = {
        "data": asr_fin_wav,
        "timestamp": time.time(),
        "type": "input_status",
    }
    output_topic = output_wav_base.format(payload["satellite"])
    msg_out = comm_tools.encrypt_msg(pld, output_topic)
    client.publish(output_topic, msg_out)
    client.loop_write()
    print("Published finish sound")
    time.sleep(hint_pause_time)


# ==================================================================================================


def on_slu_intent(client, msg):
    """Forward the slu intent to the corresponding skill, or if a skill did ask a question, check
    if the detected intent matches one of the requested and answer the question accordingly
    """
    global question_dicts, session_running_at

    payload = comm_tools.decrypt_msg(msg.payload, msg.topic)
    skill_topic = extra_tools.intent_to_topic(payload["intent"]["name"])
    pld_sound = {
        "timestamp": time.time(),
        "type": "input_status",
    }

    if payload["satellite"] not in question_dicts:
        pld_out = payload

        if payload["intent"]["name"] == "intent_not_recognized":
            pld_sound["data"] = slu_err_wav

    else:
        question_intents = question_dicts[payload["satellite"]]["question_intents"]
        question_dicts.pop(payload["satellite"])

        if question_intents[0] == "Jaco/Intents/GreedyText":
            payload["intent"]["name"] = "greedy_text"
            payload["text"] = ""
            payload["entities"] = []
            skill_topic = "Jaco/Intents/GreedyText"
            print("Updated intent to:", payload)

        pld = comm_tools.encrypt_msg(payload, skill_topic)
        pld_out = {"payload": pld.decode()}

        if payload["intent"]["name"] in question_intents:
            pld_out["topic"] = skill_topic

        elif question_intents[0] == "Jaco/Intents/GreedyText":
            pld_out["topic"] = "Jaco/Intents/GreedyText"

        else:
            pld_out["topic"] = "Jaco/Intents/IntentNotRecognized"
            pld_sound["data"] = slu_err_wav

        skill_topic = "Jaco/Skills/UserAnswer"

    if "data" in pld_sound:
        # Play error signal sound if no intent was detected
        time.sleep(hint_pause_time)
        out_wav_topic = output_wav_base.format(payload["satellite"])
        msg = comm_tools.encrypt_msg(pld_sound, out_wav_topic)
        client.publish(out_wav_topic, msg)
        client.loop_write()
        print("Published error sound")
    else:
        # An intent was detected. Sleep a short time to prevent an overlap of the asr toggle signal
        # with the skills' responses if they respond immediately
        time.sleep(hint_pause_time)

    # End satellite blocking here. Don't wait for skill responses
    # because they might take a long time or aren't responding at all
    if payload["satellite"] in session_running_at:
        session_running_at.pop(payload["satellite"])

    msg_out = comm_tools.encrypt_msg(pld_out, skill_topic)
    client.publish(skill_topic, msg_out)
    print("Sent the intent: {} to topic: '{}'".format(pld_out, skill_topic))


# ==================================================================================================


def on_say_text(client, msg):
    global question_dicts, session_running_at

    payload = comm_tools.decrypt_msg(msg.payload, msg.topic)

    if "question_intents" in payload:
        question_dict = {
            "stt_started": False,
            "question_intents": payload["question_intents"],
        }
        question_dicts[payload["satellite"]] = question_dict

    # Save satellite to be able to block wakeword detections
    session_running_at[payload["satellite"]] = time.time()

    msg_out = comm_tools.encrypt_msg(payload, output_say_topic)
    client.publish(output_say_topic, msg_out)
    print("Sent speech request:", payload)


# ==================================================================================================


def on_say_status_update(client, msg):
    global question_dicts, session_running_at

    payload = comm_tools.decrypt_msg(msg.payload, msg.topic)
    if payload["playing"] is False and payload["type"] == "speech":
        if payload["satellite"] in session_running_at:
            session_running_at.pop(payload["satellite"])

    if len(question_dicts) > 0:
        if payload["satellite"] in question_dicts:
            question_dict = question_dicts[payload["satellite"]]
            if (
                payload["playing"] is False
                and payload["type"] == "speech"
                and not question_dict["stt_started"]
            ):
                question_dict["stt_started"] = True
                activate_stt(client, payload["satellite"])


# ==================================================================================================


def main():
    global mqtt_client

    config = utils.load_global_config()
    mqtt_client = comm_tools.connect_mqtt_client(config, on_connect, on_message)
    mqtt_client.loop_forever()


# ==================================================================================================

if __name__ == "__main__":
    main()
