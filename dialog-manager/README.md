# Dialog Manager

The Dialog Manager is the central module. \
It starts the speech-to-text module if a wake word was detected. \
After a text and an intent was recognized,
it sends the intent to a skill and the skills response to the text-to-speech module.

This readme describes how to setup and debug the Dialog Manager Module.
All commands are run from `Jaco-Master` directory.

## Setup

Run the main script:

```bash
docker run --network host --rm \
  --volume `pwd`/dialog-manager/:/Jaco-Master/dialog-manager/:ro \
  --volume `pwd`/userdata/:/Jaco-Master/userdata/:ro \
  --volume `pwd`/jacolib/:/Jaco-Master/jacolib/:ro \
  --volume `pwd`/media/sounds/:/Jaco-Master/media/sounds/:ro \
  -it master_base_image_amd64 python3 /Jaco-Master/dialog-manager/action-dm.py
```

## Debugging

Test topic responses: \
(Assumes dialog-manager and mqtt-broker already running)

```bash
docker run --network host --rm \
  --volume `pwd`/dialog-manager/:/Jaco-Master/dialog-manager/:ro \
  --volume `pwd`/userdata/:/Jaco-Master/userdata/:ro \
  --volume `pwd`/jacolib/:/Jaco-Master/jacolib/:ro \
  -it master_base_image_amd64 python3 /Jaco-Master/dialog-manager/tests/test_dm.py
```
