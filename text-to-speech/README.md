# Text To Speech Module

This readme describes how to setup and debug the Text To Speech Module.

Possible TTS-Services are:

- Offline: _picotts_ (default), _mycroft_ (uncomment container build commands)
- Online: _google-translate_ (no account needed), _google-wavenet_, _amazon-polly_

All commands are run from `Jaco-Master` directory.

## Setup

Build container:

```bash
docker build -t text_to_speech_amd64 - < text-to-speech/Containerfile_amd64
```

See `speech_to_wav.sh` for instructions to setup the different services. \
Only _picotts_ and _google-translate_ will work without extra steps.

Run the main script:

```bash
docker run --network host --rm \
  --volume `pwd`/text-to-speech/:/Jaco-Master/text-to-speech/:ro \
  --volume `pwd`/text-to-speech/moduldata/cache/:/Jaco-Master/text-to-speech/moduldata/cache/ \
  --volume `pwd`/userdata/:/Jaco-Master/userdata/:ro \
  --volume `pwd`/jacolib/:/Jaco-Master/jacolib/:ro \
  -it text_to_speech_amd64 python3 /Jaco-Master/text-to-speech/action-tts.py
```

## Debugging

Connect to container:

```bash
docker run --network host --rm --ipc host --device /dev/snd \
  --volume `pwd`/text-to-speech/:/Jaco-Master/text-to-speech/:ro \
  --volume `pwd`/text-to-speech/moduldata/cache/:/Jaco-Master/text-to-speech/moduldata/cache/ \
  --volume `pwd`/userdata/:/Jaco-Master/userdata/:ro \
  --volume `pwd`/jacolib/:/Jaco-Master/jacolib/:ro \
  --volume ~/.asoundrc:/etc/asound.conf:ro \
  -it text_to_speech_amd64
```

See `speech_to_wav.sh` for instructions to debug the different tts services.

Test tts over mqtt: \
(Assumes tts-action, audio-streamer and mqtt-broker already running)

```bash
docker run --network host --rm \
  --volume `pwd`/text-to-speech/:/Jaco-Master/text-to-speech/:ro \
  --volume `pwd`/userdata/:/Jaco-Master/userdata/:ro \
  --volume `pwd`/jacolib/:/Jaco-Master/jacolib/:ro \
  -it text_to_speech_amd64 \
  python3 /Jaco-Master/text-to-speech/tests/test_publish.py "Hallo, ich kann sprechen"
```
