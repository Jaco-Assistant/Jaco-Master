#! /usr/bin/env bash

# Original script for Snips at: https://gist.github.com/DanBmh/1cf91324f1eb1ddf109937fc3bd6b577
# Shell script to handle different TextToSpeech services and online/offline connectivity

####### COMMON #####################################################################################

#------------------------------------
# Path to store downloaded voices -> No online request for already cached speeches needed
cache="/Jaco-Master/text-to-speech/moduldata/cache/"

# Some of the services create intermediate files. They are helpful for debugging problems
delete_debug_cache=true
#------------------------------------

# Debugging:
# Run script by hand and check content of cache files and temporary files
# . /Jaco-Master/text-to-speech/speech_to_wav.sh "test123.wav" "picotts" "en" "--" "--" "Hello, i am speaking to you" "--" "--" "--"
# . /Jaco-Master/text-to-speech/speech_to_wav.sh "test123.wav" "google" "en" "US" "Wavenet-D" "Hello, i am speaking to you" "16000" "1.0" "0.0"
# . /Jaco-Master/text-to-speech/speech_to_wav.sh "test123.wav" "google_translate" "en" "--" "--" "Hello, i am speaking to you" "--" "--" "--"

####### GOOGLE #####################################################################################

# Setup google account: https://cloud.google.com/text-to-speech/docs/quickstart-protocol
# Get your api key from the console https://console.developers.google.com
# Available voices: https://cloud.google.com/text-to-speech/docs/voices

googleWavenetAPIKey=""
#------------------------------------
# Uncomment the following and create a txt file with your api key and set the path accordingly:
# googleWavenetAPIKey=$(</Jaco-Master/text-to-speech/credentials/google_api_key.txt)
#------------------------------------

####################################################################################################

outfile="$1"
service="$2"
lang="$3"
country="$4"
voice="$5"
text="$6"
sampleRate="$7"
speed="$8"
pitch="$9"

if [[ "$service" == "picotts" ]]; then
  status="offline"
else
  # Test online connectivity
  
  if [[ $(wget -q --spider http://google.com) -eq 0 ]]; then
    status="online"
  else
    status="offline"
  fi
fi

# ==================================================================================================

function picotts() {
  case "$lang" in
  *en*)
    lang="en-US"
    ;;
  *de*)
    lang="de-DE"
    ;;
  *es*)
    lang="es-ES"
    ;;
  *fr*)
    lang="fr-FR"
    ;;
  *it*)
    lang="it-IT"
    ;;
  *)
    lang="en-US"
    ;;
  esac
  text=${text//'<[^>]*>'/''}
  pico2wave -w "$outfile" -l "$lang" "$text"
}

# ==================================================================================================

function offline_voice() {
    picotts
}

# ==================================================================================================

function google() {
  tmp_cache="$cache""tmp/"
  cache="$cache""google/"
  mkdir -p "$cache"
  mkdir -p "$tmp_cache"

  text=${text//\'/\\\'}

  languageCode="$lang"-"$country"
  googleVoice="$languageCode"-"$voice"

  md5string="$text"_"$googleVoice"_"$sampleRate"_"$lang"_"$speed"_"$pitch"
  hash="$(echo -n "$md5string" | md5sum | sed 's/ .*$//')"

  cachefile="$cache""$hash".wav
  downloadFile="$tmp_cache""$hash"

  if [[ -f "$cachefile" ]]; then
    cp "$cachefile" "$outfile"
  else
    if [ "$status" != "online" ]; then
      offline_voice
    else
      if [[ "$text" != *"<speak>"* ]]; then
        text="<speak>""$text""</speak>"
      fi

      curl -H "Content-Type: application/json; charset=utf-8" \
        --data "{
                'input':{
                    'ssml':'$text'
                },
                'voice':{
                    'languageCode':'$languageCode',
                    'name':'$googleVoice'
                },
                'audioConfig':{
                    'audioEncoding':'MP3',
                    'sampleRateHertz':'$sampleRate',
                    'speakingRate':'$speed',
                    'pitch':'$pitch'
                }
            }" \
        "https://texttospeech.googleapis.com/v1/text:synthesize?key="$googleWavenetAPIKey \
        >"$downloadFile"

      sed 's/audioContent//' "$downloadFile" >"$downloadFile".tmp
      tr -d '\n ":{}' < "$downloadFile".tmp > "$downloadFile".tmp2
      base64 "$downloadFile".tmp2 --decode >"$downloadFile".mp3

      mpg123 --quiet --wav "$cachefile" "$downloadFile".mp3
      cp "$cachefile" "$outfile"

      if [[ "$delete_debug_cache" != false ]]; then
        rm -r ${tmp_cache}
      fi
    fi
  fi
}

# ==================================================================================================

function google_translate() {
  tmp_cache="$cache""tmp/"
  cache="$cache""google_translate/"
  mkdir -p "$cache"
  mkdir -p "$tmp_cache"

  md5string="$text"_"$lang"
  hash="$(echo -n "$md5string" | md5sum | sed 's/ .*$//')"

  cachefile="$cache""$hash".wav
  downloadFile="$tmp_cache""$hash"

  if [[ -f "$cachefile" ]]; then
    cp "$cachefile" "$outfile"
  else
    if [ "$status" != "online" ]; then
      offline_voice
    else
      link="http://translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&tl=$lang&q=$text"
      wget -q -U Mozilla -O "$downloadFile" "$link"
      mpg123 --quiet --wav "$cachefile" "$downloadFile"
      cp "$cachefile" "$outfile"

      if [[ "$delete_debug_cache" != false ]]; then
        rm -r ${tmp_cache}
      fi
    fi
  fi
}

# ==================================================================================================

if [ "$service" = "google" ]; then
  google
elif [ "$service" = "google_translate" ]; then
  google_translate
elif [ "$service" = "picotts" ]; then
  picotts
else
  offline_voice
fi
