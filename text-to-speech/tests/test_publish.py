import argparse
import os
import time

from jacolib import comm_tools, utils

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
topic = "Jaco/SayText"

payload = {
    "data": "",
    "satellite": "Default",
    "timestamp": time.time(),
}

# ==================================================================================================


def main():
    parser = argparse.ArgumentParser(description="Test speech output")
    parser.add_argument("text", help="Text to speak")
    args = parser.parse_args()

    utils.set_repo_path(file_path + "../../")
    config = utils.load_global_config()
    client = comm_tools.connect_mqtt_client(config, None, None)

    payload["data"] = args.text
    msg = comm_tools.encrypt_msg(payload, topic)

    client.publish(topic, msg)
    client.loop()
    client.disconnect()


# ==================================================================================================

if __name__ == "__main__":
    main()
