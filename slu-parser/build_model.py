import os
import subprocess

from jacolib import utils

# ==================================================================================================

filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(filepath + "../")

workdir = filepath + "moduldata/sludata/"
nlupath = filepath + "moduldata/nlu/nlu.json"
alphabets_path = filepath + "moduldata/alphabets/"

# ==================================================================================================


def main():
    language = utils.load_global_config()["language"]
    arch_l = utils.load_architecture()
    alphabet_path = alphabets_path + "stt_{}_{}.json".format(language, arch_l)

    if utils.load_global_config()["fixed_grammar"]:
        fixed_grammar = "--fixed_grammar"
    else:
        fixed_grammar = ""

    cmd = "python3 /finstreder/finstreder/fullbuild_slu.py \
      --workdir {} --nlufile_path {}  --alphabet_path {} --order 2 {}"

    cmd = cmd.format(workdir, nlupath, alphabet_path, fixed_grammar)
    subprocess.call(["/bin/bash", "-c", cmd])


# ==================================================================================================

if __name__ == "__main__":
    main()
