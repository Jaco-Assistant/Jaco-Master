import base64
import json
import multiprocessing as mp
import os
import random
import time
import wave
from typing import List

import numpy as np
import requests

import buffer_speech
from finstreder import decoding_tools
from jacolib import comm_tools, utils

tflite_enabled = False
tf_enabled = False
try:
    import tflite_runtime.interpreter as tflite

    tflite_enabled = True
    tfl_model: tflite.Interpreter

except ImportError:
    try:
        import tensorflow as tf

        tf_enabled = True
        tf_model: tf.keras.Model

    except ImportError:
        raise ImportError(  # pylint: disable=raise-missing-from
            "No tensorflow installation could be found"
        )

# ==================================================================================================

filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(filepath + "../")

mqtt_client = None
output_topic = "Jaco/RecognizedIntent"
input_topic_base = "Jaco/{}/AudioStream"
toggle_asr_topic = "Jaco/AsrToggle"
output_wav_base = "Jaco/{}/AudioWav"
asr_running = False
vad_aggressiveness = 3

global_config = utils.load_global_config()
satellite_names = global_config["satellite_names"]
satellite_names = [s.capitalize() for s in satellite_names]
current_satellite = satellite_names[0]
input_topic = input_topic_base.format(current_satellite)

map_is = None
duckling_map = {
    "skill_dialogs-date": "time",
    "skill_dialogs-numbers2hundred": "number",
    "skill_dialogs-numbers2thousand": "number",
    "skill_dialogs-numbers_with_digits": "number",
}
extra_data_path = filepath + "moduldata/extras/"
model_params_path = filepath + "moduldata/model_params.json"
model_params = utils.load_json_file(model_params_path)
duckling_locales = model_params["locale_map"]
duckling_locale = duckling_locales[global_config["language"]]
duckling_replacers = model_params["duckling_replacers"][global_config["language"]]

alphabet_path = filepath + "moduldata/alphabets/stt_{}_{}.json"
alphabet_path = alphabet_path.format(
    global_config["language"], utils.load_architecture()
)
with open(alphabet_path, "r", encoding="utf-8") as afile:
    alphabet = json.load(afile)

fst_decoder: decoding_tools.Decoder
decoder_data = {
    "path_LGs": "/Jaco-Master/slu-parser/moduldata/sludata/results/intents/",
    "path_T": "/Jaco-Master/slu-parser/moduldata/sludata/results/token.fst",
    "path_token_syms": "/Jaco-Master/slu-parser/moduldata/sludata/symbols/tokens.syms",
    "path_word_syms": "/Jaco-Master/slu-parser/moduldata/sludata/symbols/nlu.syms",
    "alphabet": alphabet,
    "fix_alphabet": True,
    "weight_scale": 22,
    "prob_scale": 0.5,
    "filter_topk": 12,
    "mean_topk": 21,
}

stt_model_path = filepath + "moduldata/graphdata/" + global_config["language"] + "/"
if tf_enabled:
    stt_model_path += "pb/"
else:
    stt_model_path += "model_float16.tflite"


# ==================================================================================================


def init_models(tflite_model_path, decoder_datadict):
    global tf_model, tfl_model, fst_decoder

    if tf_enabled:
        tf_model = tf.keras.models.load_model(stt_model_path)
    else:
        tfl_model = tflite.Interpreter(
            model_path=tflite_model_path, num_threads=mp.cpu_count()
        )

    fst_decoder = decoding_tools.Decoder(
        decoder_datadict["path_LGs"],
        decoder_datadict["path_T"],
        decoder_datadict["path_token_syms"],
        decoder_datadict["path_word_syms"],
        decoder_datadict["alphabet"],
        fix_alphabet=decoder_datadict["fix_alphabet"],
        weight_scale=decoder_datadict["weight_scale"],
        prob_scale=decoder_datadict["prob_scale"],
        filter_topk=decoder_datadict["filter_topk"],
        mean_topk=decoder_datadict["mean_topk"],
    )

    print("Initializing STT model ...")
    for _ in range(5):
        length = random.randint(12340, 345678)
        data = np.random.uniform(-1, 1, [1, length]).astype(np.float32)
        if tf_enabled:
            _ = tf_model.predict(data, steps=1)[0]
        else:
            _ = predict_signal(data)


# ==================================================================================================


def predict_signal(audio):
    global tfl_model

    interpreter = tfl_model
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    # Enable dynamic shape inputs
    interpreter.resize_tensor_input(input_details[0]["index"], audio.shape)
    interpreter.allocate_tensors()

    interpreter.set_tensor(input_details[0]["index"], audio)
    interpreter.invoke()

    output_data = interpreter.get_tensor(output_details[0]["index"])
    output_data = output_data[0]
    return output_data


# ==================================================================================================


def decode_greedy_text(ctc_input: List[List[float]]) -> str:
    """Greedy ctc to text decoding"""

    # Merge repeated characters
    values = np.argmax(ctc_input, axis=-1)
    merged = [values[0]]
    for v in values[1:]:
        if v != merged[-1]:
            merged.append(v)

    blank_id = (len(alphabet) + 1) - 1
    merged = [v for v in merged if not v == blank_id]
    gd_text = "".join([alphabet[v] for v in merged])
    return gd_text


# ==================================================================================================


def save_debug_wav(audio):
    fp = filepath + "debug/" + str(int(time.time() * 1000)) + ".wav"

    if not os.path.exists(os.path.dirname(fp)):
        os.makedirs(os.path.dirname(fp))

    # Delete all but the most recent wav files
    files = sorted(os.listdir(os.path.dirname(fp)))
    files = [f for f in files if f.endswith(".wav")]
    for f in files[:-2]:
        os.remove(os.path.dirname(fp) + "/" + f)

    wf = wave.open(fp, "wb")
    wf.setnchannels(1)
    wf.setsampwidth(2)
    wf.setframerate(16000)
    wf.writeframes(audio.tobytes())
    wf.close()


# ==================================================================================================


def decode_prediction(prediction):
    global fst_decoder

    intent = fst_decoder.decode2intent(ctc_input=prediction.tolist())
    return intent


# ==================================================================================================


def decode_audio(audio):
    global tf_model

    # Save the audio to a wav file
    save_debug_wav(audio)

    # Normalize the audio
    audio = audio / (np.iinfo(np.int16).max + 1)
    audio = np.expand_dims(audio, axis=0)
    audio = audio.astype(np.float32)

    stime = time.time()
    if tf_enabled:
        prediction = tf_model.predict(audio, steps=1)[0]
    else:
        prediction = predict_signal(audio)
    print("Greedy: '" + decode_greedy_text(prediction.tolist()) + "'")

    intent = decode_prediction(prediction)
    print("Transcription took: {:.3f}s".format(time.time() - stime))

    return intent


# ==================================================================================================


def on_connect(client):
    # Subscribe to all possible satellites
    for c in satellite_names:
        client.subscribe(input_topic_base.format(c))

    client.subscribe(toggle_asr_topic)


# ==================================================================================================


def on_message(client, userdata, msg):
    global asr_running, input_topic, current_satellite

    if msg.topic == toggle_asr_topic:
        print("Received a toggle request")
        payload = comm_tools.decrypt_msg(msg.payload, msg.topic)

        if not payload["toggle"] is True:
            asr_running = False
            return

        buffer_speech.empty_buffers()
        asr_running = payload["toggle"]
        current_satellite = payload["satellite"]
        input_topic = input_topic_base.format(current_satellite)

    if msg.topic == input_topic:
        # Append the incoming audio frame to the buffer
        # Ignoring all satellites except the one where the wake word was triggered

        payload = comm_tools.decrypt_msg(msg.payload, msg.topic)
        frames = payload["data"].encode()
        frames = base64.b64decode(frames)
        frames = np.frombuffer(frames, dtype=np.int16)
        buffer_speech.add_to_buffer(frames)


# ==================================================================================================


def duckling_parse(text: str):
    url = "http://0.0.0.0:8008/parse"
    payload = {"locale": duckling_locale, "text": text}

    try:
        res = requests.post(url, data=payload, headers={}, timeout=0.5)

        value = json.loads(res.text)
        value = value[0]["value"]
        if "values" in value:
            value = value["values"][0]["value"]
        else:
            value = value["value"]
        return value

    except (
        requests.exceptions.ReadTimeout,
        json.decoder.JSONDecodeError,
        KeyError,
        IndexError,
    ) as e:
        print(e)
        return ""


# ==================================================================================================


def update_intent(parsed_intent: dict) -> dict:

    intent = {
        "intent": {
            "name": parsed_intent["intent"]["name"],
        },
        "text": parsed_intent["text"],
        "greedy": parsed_intent["greedy"],
        "entities": [],
    }

    if parsed_intent["text"] == "":
        intent["intent"]["name"] = "intent_not_recognized"
        return intent

    for entity in parsed_intent["entities"]:
        if entity["entity"] in duckling_map:
            value = entity["value"]

            # Replace special words for duckling extractor. For example without german umlauts
            #  the word "zwoelf" is not detected, so it's replaced with the correct spelling "zwölf"
            for c in duckling_replacers.keys():
                value = value.replace(c, duckling_replacers[c])

            value = duckling_parse(value)

            if entity["entity"] == "skill_dialogs-date":
                # Keep only the date part drop the time values
                value = value[0:10]

            entity["value"] = value

        intent["entities"].append(entity)

    return intent


# ==================================================================================================


def on_finished_buffer(audio):
    global asr_running

    # Send toggle false message to inform listeners about this event. It would be possible to listen
    # to the text transcription message instead, but it would require extra topic reading permission
    payload = {
        "toggle": False,
        "satellite": current_satellite,
    }
    msg_out = comm_tools.encrypt_msg(payload, toggle_asr_topic)
    mqtt_client.publish(toggle_asr_topic, msg_out)
    mqtt_client.loop_write()
    print("Published a toggle request:", payload)

    intent = decode_audio(audio)
    payload = update_intent(intent)
    payload["satellite"] = current_satellite
    payload["timestamp"] = time.time()

    msg = comm_tools.encrypt_msg(payload, output_topic)
    mqtt_client.publish(output_topic, msg)
    print("Detected intent:", payload)

    # Disable asr again until there is a new activation request
    asr_running = False


# ==================================================================================================


def main():
    global mqtt_client, map_is

    with open(extra_data_path + "intent_slot_map.json", "r", encoding="utf-8") as file:
        map_is = json.load(file)

    timeout = float(global_config["speech_transcription_timeout"])
    buffer_speech.init(vad_aggressiveness, timeout)

    init_models(stt_model_path, decoder_data)
    config = utils.load_global_config()
    mqtt_client = comm_tools.connect_mqtt_client(config, on_connect, on_message)

    print("Started listening for speech ...")
    while True:
        if asr_running:
            audio = buffer_speech.run_slu_step()
            if audio is not None:
                on_finished_buffer(audio)

        # The timeout has to be lower than the vad_buffer_size in buffer_speech if the audio packet
        # size is bigger than the vad_buffer_size, because else the cache is not cleared fast enough
        mqtt_client.loop(timeout=0.01)


# ==================================================================================================

if __name__ == "__main__":
    main()
