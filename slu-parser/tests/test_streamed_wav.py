import argparse
import base64
import os
import time

import numpy as np
import soundfile
import tqdm

from jacolib import comm_tools, utils

# ==================================================================================================

filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(filepath + "../../")

output_satellite = "Default"
sample_rate = 16000
buffer_duration = 0.030
buffer_size = int(sample_rate * buffer_duration)
output_topic = "Jaco/{}/AudioStream"
output_topic = output_topic.format(output_satellite)
toggle_asr_topic = "Jaco/AsrToggle"


# ==================================================================================================


def stream_wav(client, path_wav):
    # Activate the speech to text module
    payload = {
        "toggle": True,
        "satellite": output_satellite,
        "timestamp": time.time(),
    }
    msg = comm_tools.encrypt_msg(payload, toggle_asr_topic)

    # Publish and wait some time while the start sound is played
    client.publish(toggle_asr_topic, msg)
    client.loop()
    time.sleep(1)

    wav_data, _ = soundfile.read(path_wav, dtype="int16")

    # Add one and a half second of zero noise, to ensure an end of speech is detected
    # If a audiofile has sounds at the end the program will wait for more speech to come
    wav_data = np.pad(wav_data, (0, int(sample_rate * 1.5)), "constant")

    # Play the sample array in chunks
    idx = 0
    wav_length = len(wav_data)
    pbar = tqdm.tqdm(total=wav_length)
    while idx < wav_length:
        start_time = time.time()
        old_idx = idx
        idx = min(idx + buffer_size, wav_length)
        buff = wav_data[old_idx:idx]
        sbuff = base64.b64encode(buff.tobytes()).decode()

        payload = {
            "data": sbuff,
            "timestamp": time.time(),
        }
        msg = comm_tools.encrypt_msg(payload, output_topic)
        client.publish(output_topic, msg)
        client.loop()

        # Sleep to ensure the file is streamed in realtime
        sleep_time = max(0.0, buffer_duration - (time.time() - start_time))
        time.sleep(sleep_time)
        pbar.update(len(buff))


# ==================================================================================================


def main():
    parser = argparse.ArgumentParser(description="Test streaming slu with a file")
    parser.add_argument(
        "--path_wav", required=True, help="Path of the mono-channel file.wav to test"
    )
    args = parser.parse_args()

    config = utils.load_global_config()
    client = comm_tools.connect_mqtt_client(config, None, None)
    stream_wav(client, args.path_wav)


# ==================================================================================================

if __name__ == "__main__":
    main()
