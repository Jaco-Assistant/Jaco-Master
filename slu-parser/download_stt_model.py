import argparse
import os
import subprocess
import zipfile

import requests

from jacolib import utils

# ==================================================================================================

filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(filepath + "../")
moduldata_dir = filepath + "moduldata/"
model_params_path = moduldata_dir + "model_params.json"

# ==================================================================================================


def download_file_from_google_drive(gd_id, destination):
    """Taken from: https://stackoverflow.com/a/39225039"""

    def get_confirm_token(response):
        for key, value in response.cookies.items():
            if key.startswith("download_warning"):
                return value
        return None

    def save_response_content(response, dest):
        chunk_size = 32768
        with open(dest, "wb") as f:
            for chunk in response.iter_content(chunk_size):
                if chunk:
                    # filter out keep-alive new chunks
                    f.write(chunk)

    url = "https://docs.google.com/uc?export=download"
    session = requests.Session()
    resp = session.get(url, params={"id": gd_id}, stream=True)
    token = get_confirm_token(resp)

    if token:
        params = {"id": gd_id, "confirm": token}
        resp = session.get(url, params=params, stream=True)

    save_response_content(resp, destination)


# ==================================================================================================


def download_file_from_mediafire(share_link, destination):
    """Based upon: https://superuser.com/a/1517096"""

    # Using curl on Raspi didn't retrieve the link, so it was replaced with wget
    url = 'wget -qO- "{}"|grep "href.*download.*mediafire.*"|sed \'s/.*href="\\([^ ]*\\)".*/\\1/\''
    url = url.format(share_link)

    cmd = "wget -O {} $({})".format(destination, url)
    print(cmd)
    subprocess.call(["/bin/bash", "-c", cmd])


# ==================================================================================================


def download_file(link: str, target: str):

    if "drive.google.com" in link:
        # Can not use wget with google drive
        gdid = link.replace("https://drive.google.com/file/d/", "")
        gdid = gdid.replace("/view", "")
        download_file_from_google_drive(gdid, target)
    elif "mediafire.com" in link:
        # Can not use wget with mediafire directly
        download_file_from_mediafire(link, target)
    else:
        cmd = "wget -O {} {}".format(target, link)
        subprocess.call(["/bin/bash", "-c", cmd])

    if target.endswith(".zip"):
        # Extract zipped directory
        with zipfile.ZipFile(target, "r") as zfile:
            zfile.extractall(path=os.path.dirname(target))
        os.remove(target)


# ==================================================================================================


def download_stt_model(lang=""):

    if lang == "":
        language = utils.load_global_config()["language"]
    else:
        # Needed for testing purposes
        language = lang

    arch_l = utils.load_architecture()
    graph_link = utils.load_json_file(model_params_path)["checkpoints"][language][
        arch_l
    ]
    albet_link = utils.load_json_file(model_params_path)["alphabets"][language][arch_l]

    modeldir = moduldata_dir + "graphdata/" + language + "/"
    if not os.path.isdir(modeldir):
        os.makedirs(modeldir)

    albetdir = moduldata_dir + "alphabets/"
    if not os.path.isdir(albetdir):
        os.makedirs(albetdir)
    albet_path = albetdir + "stt_{}_{}.json".format(language, arch_l)

    if arch_l == "arm64":
        graph_path = modeldir + "model_float16.tflite"
    else:
        graph_path = modeldir + "pb.zip"

    print("\nDownloading STT model ...")
    download_file(graph_link, graph_path)
    download_file(albet_link, albet_path)

    print("Finished download \n")


# ==================================================================================================


def main():
    parser = argparse.ArgumentParser(description="Get STT model")
    parser.add_argument(
        "--language",
        required=False,
        default="",
        help="Download the STT model in given language",
    )
    args = parser.parse_args()

    download_stt_model(args.language)


# ==================================================================================================

if __name__ == "__main__":
    main()
