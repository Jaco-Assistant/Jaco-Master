import os

import basic_normalizer
from jacolib import utils

# ==================================================================================================

filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
langdicts = None

# ==================================================================================================


def get_langdicts() -> dict:
    """Load the langdicts, or just return them if they are already loaded"""
    global langdicts

    if langdicts is None:
        path = filepath + "../slu-parser/moduldata/langdicts.json"
        ld: dict = utils.load_json_file(path)
        langdicts = ld

        # Add empty replacers
        langs = list(langdicts["allowed_chars"].keys())
        for lang in langs:
            if lang not in langdicts["replacers"]:
                langdicts["replacers"][lang] = {}

    return langdicts


# ==================================================================================================


def clean_text_for_stt(text: str, language: str) -> str:
    """Clean a text that it only consists of chars used in stt.
    Moved here because this may be useful for some skills too."""

    lds = get_langdicts()
    allowed_lang = lds["allowed_chars"][language]
    replacer_lang = lds["replacers"][language]

    text = clean_text(text, replacer_lang, allowed_lang)
    return text


# ==================================================================================================


def clean_text(text: str, replacers: dict, allowed: str) -> str:
    """Clean a text by replacing and removing characters,
    so that it only consists of the given allowed chars"""

    text = text.lower()
    for c in replacers.keys():
        text = text.replace(c, replacers[c])

    normalizr = basic_normalizer.Normalizer(allowed_chars=list(set(allowed)))
    text = normalizr.clean_sentence(text)

    return text
