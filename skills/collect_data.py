import os
import re

import text_tools
from jacolib import utils

# pylint: disable=anomalous-backslash-in-string

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(file_path + "../")

nlu_collection_path = file_path + "collected/nlu/"

allowed_default = '0123456789#_`: .-\[\]\(\)"\{\}|?>'  # noqa: W605
nlu_prefix = ["## intent:", "## lookup:"]


# ==================================================================================================


def clean_content(text):
    """Replace or delete not allowed characters and remove comments"""

    text = text.lower()

    # Remove spaces behind text
    lines = text.split("\n")
    lines = [line.strip() for line in lines]
    text = "\n".join(lines)

    # Replace entity text links
    text = re.sub(r".txt(?!\n)", "", text)

    # Replace all dots, except those in lookup.txt definitions
    text = re.sub(r"[.](?!txt)", "", text)

    # Replace all question marks except those for role definitions (followed by a bracket)
    text = re.sub(r"\?(?![a-z_0-9]+\))", "", text)

    # Replace all dashes except those in slot definitions, at the beginning of the sentence,
    # or in synonym replacements
    text = re.sub(r"(?<=.)-(?!([a-z_0-9?]+\))|>)", " ", text)

    # Replace all colons except those for intents and lookups
    text = re.sub(r"((?<!## intent)((?<!## lookup))):", " ", text)

    # Replace or delete all not allowed chars
    language = utils.load_global_config()["language"]
    allowed_lang = text_tools.get_langdicts()["allowed_chars"][language]
    replacer_lang = text_tools.get_langdicts()["replacers"][language]
    allowed_rgx = allowed_default + allowed_lang
    text = "\n".join(
        [text_tools.clean_text(t, replacer_lang, allowed_rgx) for t in text.split("\n")]
    )

    # Delete comments in nlu-files
    lines = text.split("\n")
    lines = [ln for ln in lines if not ln.startswith("`")]
    lines = [ln for ln in lines if not ln.startswith("- `")]
    text = "\n".join(lines)

    # Delete comments in lookup-files
    lines = text.split("\n")
    lines = [
        line for line in lines if line.startswith("##") or not line.startswith("#")
    ]
    text = "\n".join(lines)

    # Replace multiple whitespaces
    lines = text.split("\n")
    lines = [re.sub(r"\s+", " ", line) for line in lines]
    text = "\n".join(lines)

    return text


# ==================================================================================================


def rename_items(text, skill_prefix):
    """Rename nlu items to have the skill name as prefix"""

    text = text.lower()

    # Rename intents
    for p in nlu_prefix:
        text = text.replace(p, p + skill_prefix + "-")

    # Rename entities
    rpl = "](" + skill_prefix + "-"
    text = re.sub(r"]\((?!skill_dialogs-)", rpl, text)

    # Rename lookup files
    rpl = "\g<1>\n" + skill_prefix + "-"  # noqa: W605
    text = re.sub(r"(## lookup:[a-z0-9_-]+)\s*\n\s*", rpl, text)

    return text


# ==================================================================================================


def process_file_content(path, skill_prefix):
    with open(path, "r", encoding="utf-8") as file:
        content = file.read()

    content = clean_content(content)
    content = rename_items(content, skill_prefix)
    return content


# ==================================================================================================


def main():
    language = utils.load_global_config()["language"]

    # Delete and recreate data directories
    utils.empty_with_ignore(nlu_collection_path)

    # Copy dialog data for each skill
    skills_with_paths = utils.get_skills_with_paths()
    print("Found {} skills".format(len(skills_with_paths)))

    for skill, skill_path in skills_with_paths:
        skill_prefix = utils.skill_to_prefix(skill)

        nlu_path = skill_path + "/dialog/nlu/" + language + "/"
        if os.path.isdir(nlu_path):
            for file_name in os.listdir(nlu_path):
                content = process_file_content(nlu_path + file_name, skill_prefix)

                path = nlu_collection_path + skill_prefix + "-" + file_name.lower()
                with open(path, "w+", encoding="utf-8") as file:
                    file.write(content)
        else:
            print("Skill '{}' has no nlu data".format(skill))


# ==================================================================================================

if __name__ == "__main__":
    main()
