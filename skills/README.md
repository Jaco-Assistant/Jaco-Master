# Skills

Tools to handle dialog data shipped with the skills.

<br>

Collect and prepare data :

```bash
docker run --network host --rm \
  --volume `pwd`/skills/:/Jaco-Master/skills/ \
  --volume `pwd`/userdata/:/Jaco-Master/userdata/:ro \
  --volume `pwd`/jacolib/:/Jaco-Master/jacolib/:ro \
  -it master_base_image_amd64 /bin/bash -c '\
    python3 /Jaco-Master/skills/collect_data.py &&  \
    python3 /Jaco-Master/skills/map_intents_slots.py && \
    python3 /Jaco-Master/skills/generate_data.py'
```

Run test:

```bash
pytest skills/ -vv
```
