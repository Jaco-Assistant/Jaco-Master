# Broker Module

The modules use mqtt for communication.
This readme describes how to setup and debug the Broker Module.

All commands are run from `Jaco-Master` directory.

## Setup

Build container:

```bash
docker build -t mqtt_broker_amd64 - < mqtt-broker/Containerfile_amd64
```

Run the main script:

```bash
docker run --network host --rm \
  --volume `pwd`/mqtt-broker/:/Jaco-Master/mqtt-broker/:ro \
  -it mqtt_broker_amd64 /bin/bash -c "mosquitto -c /Jaco-Master/mqtt-broker/mosquitto.conf"
```

## Debugging

Connect to container:

```bash
docker run --network host --rm \
  --volume `pwd`/mqtt-broker/:/Jaco-Master/mqtt-broker/ \
  -it mqtt_broker_amd64
```

Change the mqtt authentication:

```bash
# Run in container
touch /tmp/passwd && mosquitto_passwd -b /tmp/passwd [username] [password]
cat /tmp/passwd > /Jaco-Master/mqtt-broker/passwd && rm /tmp/passwd
```
