# Testing

Build container with testing tools:

```bash
docker build -t testing_jaco_mtr - < tests/Containerfile

docker run --network host --rm \
  --volume `pwd`/:/Jaco-Master/ \
  -it testing_jaco_mtr
```

Execute unit tests:

```bash
# Run in container
cd /Jaco-Master/ && pytest --cov-config=setup.cfg --cov skills/
```

For syntax tests, check out the steps in the [gitlab-ci](../.gitlab-ci.yml#L69) file.
