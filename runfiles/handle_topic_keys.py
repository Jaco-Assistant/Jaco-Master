import argparse
import json
import os
from typing import List

from cryptography.fernet import Fernet

from jacolib import comm_tools, extra_tools, utils

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(file_path + "../")


# ==================================================================================================


def get_skill_topics(skill_path: str) -> List[str]:
    """Collect all topics a skill reads from or writes to"""

    all_tops = []
    skill_config = utils.load_skill_config(skill_path)

    tops = skill_config["system"]["topics_read"]
    if tops is not None:
        all_tops.extend(tops)
    tops = skill_config["system"]["topics_write"]
    if tops is not None:
        all_tops.extend(tops)

    return all_tops


# ==================================================================================================


def generate_topic_keys(modules_only=False, skills_only=False):
    """Generate password keys to encrypt the payload of all mqtt topics"""

    topics_satellites = [
        "Jaco/{}/AudioWav",
        "Jaco/StreamWav/Status",
        "Jaco/{}/AudioStream",
        "Jaco/WakeWord",
    ]
    topics_master = [
        "Jaco/AsrToggle",
        "Jaco/RecognizedIntent",
        "Jaco/SayText",
        "Jaco/Intents/IntentNotRecognized",
        "Jaco/Intents/GreedyText",
    ]

    # Insert satellite names
    top_sat = []
    sats = utils.load_global_config()["satellite_names"]
    for t in topics_satellites:
        if "{}" in t:
            for s in sats:
                ts = t.format(s.capitalize())
                top_sat.append(ts)
        else:
            top_sat.append(t)
    topics_satellites = top_sat

    # Get skill topics
    topics_skills = []
    skills_with_paths = utils.get_skills_with_paths()
    for skill, skill_path in skills_with_paths:
        tops = get_skill_topics(skill_path)

        for i, t in enumerate(list(tops)):
            t = convert_custom_intents_to_topic(t, skill)
            tops[i] = t
        topics_skills.extend(tops)

    all_topics = set(topics_master + topics_satellites + topics_skills)
    module_topics = set(topics_master + topics_satellites)

    # Split keys into module and skill keys, because later ones change more often
    module_keys = {}
    skill_keys = {}
    for t in sorted(all_topics):
        k = Fernet.generate_key().decode()
        if t in module_topics:
            module_keys[t] = k
        else:
            skill_keys[t] = k

    if not skills_only:
        path = file_path + "../userdata/module_topic_keys.json"
        with open(path, "w+", encoding="utf-8") as file:
            json.dump(module_keys, file, indent=2)

    if not modules_only:
        path = file_path + "../userdata/skill_topic_keys.json"
        with open(path, "w+", encoding="utf-8") as file:
            json.dump(skill_keys, file, indent=2)


# ==================================================================================================


def convert_custom_intents_to_topic(t, skill_name):
    if not t.startswith("Jaco/"):
        # Convert custom skill intents to topic names
        prefix = utils.skill_to_prefix(skill_name)
        t = prefix + "-" + t
        t = extra_tools.intent_to_topic(t)
    elif t == "Jaco/Skills/SayText":
        # Add skill suffix to SayText topic
        prefix = utils.skill_to_prefix(skill_name)
        t = t + "".join(x.capitalize() for x in prefix.split("_"))
    return t


# ==================================================================================================


def distribute_topic_keys():
    """Copy topic keys to the skills. But only those the skills listed in their config files"""

    topic_keys = comm_tools.load_topic_keys()

    skills_with_paths = utils.get_skills_with_paths()
    for skill, skill_path in skills_with_paths:
        tops = get_skill_topics(skill_path)

        skill_keys = {}
        for t in tops:
            t = convert_custom_intents_to_topic(t, skill)
            skill_keys[t] = topic_keys[t]

        if skill_keys != {}:
            sd_path = skill_path + "/skilldata/"
            if not os.path.isdir(sd_path):
                utils.empty_with_ignore(sd_path)

            path = skill_path + "/skilldata/topic_keys.json"
            with open(path, "w+", encoding="utf-8") as file:
                json.dump(skill_keys, file, indent=2)


# ==================================================================================================


def main():
    parser = argparse.ArgumentParser(description="Install Jaco-Master modules")
    parser.add_argument(
        "--generate_module_keys",
        action="store_true",
        help="Generate new topic encryption keys for the modules",
    )
    parser.add_argument(
        "--generate_skill_keys",
        action="store_true",
        help="Generate new topic encryption keys for the skills",
    )
    parser.add_argument(
        "--distribute_keys",
        action="store_true",
        help="Distribute new topic encryption keys",
    )
    args = parser.parse_args()

    if not any(list(vars(args).values())):
        print("Run with '--help' or '-h' for argument explanations")

    if args.generate_module_keys:
        generate_topic_keys(modules_only=True)

    if args.generate_skill_keys:
        generate_topic_keys(skills_only=True)

    if args.distribute_keys:
        distribute_topic_keys()


# ==================================================================================================

if __name__ == "__main__":
    main()
